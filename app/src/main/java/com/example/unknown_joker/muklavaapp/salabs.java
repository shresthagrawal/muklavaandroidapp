package com.example.unknown_joker.muklavaapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by Unknown-Joker on 4/13/2018.
 */

public class salabs {
    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }
    public static String changedateformat(String date) throws ParseException {
        Date initDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String parsedDate = formatter.format(initDate);
        return parsedDate;
    }

    public static int getIntDefaults(String key, Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(isInteger(preferences.getString(key, null)))
        {
            return Integer.parseInt(preferences.getString(key, null));
        }
        else
        {
            return -1;
        }

    }

    static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public static String[][] sqlquery(Context ctx,String querry)
    {
        salabssqlconnector backgroundTask=new salabssqlconnector(ctx);
        backgroundTask.execute(querry,"localhost","Muklava","muklava","shresthharsh");
        String rawdata= salabs.getDefaults("rawdata",ctx);
        return getArray(rawdata,ctx);

    }

    public static ArrayList<String[]> getArrayList(final String raw, final Context ctx)
    {

        try {


            int counterx = 0, countery=0, x=0;

            for( int i=0; i<raw.length(); i++ ) {
                if( raw.charAt(i) == '|' && x==0 ) {
                    counterx++;
                }
                else if(raw.charAt(i)=='!')
                {
                    countery++;
                    x=1;
                }
            }
            //salabs.setDefaults("sizerow",Integer.toString(countery),ctx);

            ArrayList<String[]> value = new ArrayList<String[]>();
            //String[][] value = new String[countery][counterx];
            String after= raw;

            for( int i=0; i<countery; i++ ) {
                String[] row =new String [counterx];
                for( int j=0; j<counterx; j++ ) {

                    row[j] = after.substring(0, after.indexOf('|'));
                    String v = after.substring(after.indexOf('|') + 1);
                    after = v;
                }
                value.add(row);
                after = after.substring(after.indexOf('!') + 1);
            }


            return value;}
        catch (ArithmeticException e)
        {
            String nll[][]= {{"",""},{"",""}};
            return null;
        }
        catch (Exception e)
        {
            String nll[][]= {{"",""},{"",""}};
            return null;
        }
    }


    public static String[][] getArray(final String raw, final Context ctx)
    {
        try {


        int counterx = 0, countery=0, x=0;

        for( int i=0; i<raw.length(); i++ ) {
            if( raw.charAt(i) == '|' && x==0 ) {
                counterx++;
            }
            else if(raw.charAt(i)=='!')
            {
                countery++;
                x=1;
            }
        }
        //salabs.setDefaults("sizerow",Integer.toString(countery),ctx);


        String[][] value = new String[countery][counterx];
        String after= raw;

        for( int i=0; i<countery; i++ ) {
            for( int j=0; j<counterx; j++ ) {

                value[i][j] = after.substring(0, after.indexOf('|'));
                String v = after.substring(after.indexOf('|') + 1);
                after = v;
            }
          after = after.substring(after.indexOf('!') + 1);
        }


        return value;}
        catch (ArithmeticException e)
        {
            String nll[][]= {{"",""},{"",""}};
            return nll;
        }
        catch (Exception e)
        {
            String nll[][]= {{"",""},{"",""}};
            return nll;
        }
    }




}
