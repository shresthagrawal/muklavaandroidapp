package com.example.unknown_joker.muklavaapp.Bill;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.salabs;
import com.squareup.picasso.Picasso;

public class PhotoView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view =getLayoutInflater().inflate(R.layout.activity_photo_view,null); // get reference to root activity view
        setContentView(view);

        view.setOnClickListener(new View.OnClickListener() {
            float zoomFactor = 2f;
            boolean zoomedOut = false;

            @Override
            public void onClick(View v) {
                if(zoomedOut) {
                    v.setScaleX(1);
                    v.setScaleY(1);
                    zoomedOut = false;
                }
                else {
                    v.setScaleX(zoomFactor);
                    v.setScaleY(zoomFactor);
                    zoomedOut = true;
                }
            }
        });

        ImageView imageView = (ImageView) findViewById(R.id.photo);

        int id = salabs.getIntDefaults("PhotoId",this);
        try {
            Picasso.with(this).load("http://muklava.in/muklava/"+id+".jpeg").into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
