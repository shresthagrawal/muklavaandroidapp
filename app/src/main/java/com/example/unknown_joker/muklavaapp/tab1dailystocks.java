package com.example.unknown_joker.muklavaapp;

/**
 * Created by Unknown-Joker on 4/19/2018.
 */
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.CustomListAdapter;
import com.example.unknown_joker.muklavaapp.Product;
import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.salabs;
import com.example.unknown_joker.muklavaapp.salabsofflinedbms;

import java.util.ArrayList;


public class tab1dailystocks extends Fragment{
    ViewPager viewPager;
    public ArrayList itemname=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab1dailystocks, container, false);

        final ArrayList arrayList = new ArrayList<>();
        ListView lv = (ListView) rootView.findViewById(R.id.listviewitemname);

        salabsofflinedbms salabsofflinedbms = new salabsofflinedbms(getContext());
        Cursor csr = salabsofflinedbms.query("select productname, count(productname) from product where (branch='"+ salabs.getDefaults("branch",getContext())+"') group by productname ");
        while(csr.moveToNext()) {
             itemname.add(new String ((String) csr.getString(0)));
            arrayList.add(new Product("http://muklava.in/muklava/"+""+".jpeg",csr.getString(0),csr.getString(1) ));
        }

        CustomListAdapter adapter = new CustomListAdapter(
               getActivity().getApplicationContext(), R.layout.custom_list_layout, arrayList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                Toast.makeText(getContext(), Integer.toString(position), Toast.LENGTH_LONG).show();
                salabs.setDefaults("itemnametodaysstock",itemname.get(position).toString(),getContext());
                viewPager = (ViewPager) getActivity().findViewById(
                        R.id.container);
                viewPager.setCurrentItem(1);
            }
        });
        /**/

        return rootView;



    }



}
