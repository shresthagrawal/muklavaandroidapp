package com.example.unknown_joker.muklavaapp.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.unknown_joker.muklavaapp.Bill.BillView;
import com.example.unknown_joker.muklavaapp.Bill.ListOfBill;
import com.example.unknown_joker.muklavaapp.Bill.ListView.element;
import com.example.unknown_joker.muklavaapp.Bill.ListView.listViewAdapter;
import com.example.unknown_joker.muklavaapp.CreateOrder;
import com.example.unknown_joker.muklavaapp.FireBase.MyFirebaseInstanceIDService;
import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.customerentry;
import com.example.unknown_joker.muklavaapp.dailystockmanager;
import com.example.unknown_joker.muklavaapp.photolistview;
import com.example.unknown_joker.muklavaapp.salabs;
import com.example.unknown_joker.muklavaapp.salabssqlconnector;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Statistics extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    salabssqlconnector salabssqlconnector1= new salabssqlconnector(this);
    ListView lv;
    ArrayList<element> arrayList;
    String data[][];
    String date="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        date = df.format(c);
        populateTheBillListView(date);
       // dateSelected(date);




        salabssqlconnector.Command cmd = new salabssqlconnector.Command(){
            public void execute(String response){
                GraphView graph = (GraphView) findViewById(R.id.graph);
                String data[][];
                Toast.makeText(Statistics.this,response,Toast.LENGTH_LONG).show();
                data = salabs.getArray(response,Statistics.this);
                int size = data.length;
               DataPoint dp[] = new DataPoint[data.length];
                String date[] = new String[data.length];

                for(int i=0; i<size;i++) {


                        dp[i]= new DataPoint(Integer.parseInt(data[i][0]),Integer.parseInt(data[i][1]));
                        date[i]=data[i][2];

                }

                LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);
                graph.addSeries(series);

            }
        };

        final salabssqlconnector salabssqlconnector= new salabssqlconnector(this,cmd);
        salabssqlconnector.execute("SELECT Day(CurrentDateTime),sum(TotalPrice),CurrentDateTime FROM `CustomerTransiction` WHERE Month(CurrentDateTime) = Month(CURDATE()) group by Day(CurrentDateTime)","localhost","muklavalocal","muklava","shresthharsh");
              //*/
        CalendarView view = findViewById(R.id.calendarView);

        view.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView arg0, int year, int month,
                                            int date) {

                Toast.makeText(Statistics.this,year+ "-"+(month+1)+"-"+date,Toast.LENGTH_LONG).show();
                populateTheBillListView(year+ "-"+(month+1)+"-"+date);
            }
        });



        /*graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return super.formatLabel(value, isValueX);
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX) + " ";
                }
            }
        });
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(new String[] {"old", "middle", "new"});
        staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);//*/






    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.statistics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            MyFirebaseInstanceIDService idfb = new MyFirebaseInstanceIDService();
            idfb.onTokenRefresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.createcustomer) {
            Intent i = new Intent(this, customerentry.class);
            startActivity(i);

        }
        else if (id == R.id.Bill) {

            Intent i = new Intent(this, ListOfBill.class);
            startActivity(i);


        }else if (id == R.id.nav_gallery) {
            salabs.setDefaults("queryresult","",this);
            Intent i = new Intent(this, photolistview.class);
            startActivity(i);


        } else if (id == R.id.createorder) {
            Intent i = new Intent(this, CreateOrder.class);
            startActivity(i);


        } else if (id == R.id.nav_manage) {
            Intent test = new Intent(this, dailystockmanager.class);
            startActivity(test);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    int id;
    void populateTheBillListView(final String datex)
    {
        lv = (ListView) findViewById(R.id.BillList);
        arrayList = new ArrayList<>();

        salabssqlconnector.Command cmd = new salabssqlconnector.Command(){
            public void execute(String response){
                //do something
                data = salabs.getArray(response,Statistics.this);

                int size = data.length;
                Toast.makeText(Statistics.this, "total number:" + Integer.toString(data.length), Toast.LENGTH_LONG).show();
              //  RelativeLayout.LayoutParams mParam = new RelativeLayout.LayoutParams((int)(400),(int)(size*100));
              //  lv.setLayoutParams(mParam);
                for(int i=0; i<size;i++) {

                    arrayList.add(new element(data[i][1]+":"+data[i][0],data[i][3]+"/-" ));
                }
                listViewAdapter adapter = new listViewAdapter(Statistics.this, R.layout.activity_list_view_adapter, arrayList);
                lv.setAdapter(adapter);
                dateSelected(datex);
            }
        };
        final salabssqlconnector salabssqlconnector= new salabssqlconnector(this,cmd);

        Toast.makeText(this,date,Toast.LENGTH_LONG).show();
        salabssqlconnector.execute("SELECT Number, Name, TransictionId, TotalPrice, credit FROM CustomerTransiction WHERE  Date(CurrentDateTime) =('"+datex+"') ORDER BY CurrentDateTime DESC ","localhost","muklavalocal","muklava","shresthharsh");






        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(ListOfBill.this, "total number:" + position, Toast.LENGTH_LONG).show();
                salabs.setDefaults("BillId",data[position][2],Statistics.this);
                Intent intent = new Intent(Statistics.this, BillView.class);
                startActivity(intent);
            }});


    }



    public void dateSelected(String s) {
            salabssqlconnector.Command cmd = new salabssqlconnector.Command(){
            public void execute(String response){
                TextView  dailySale = (TextView)findViewById(R.id.dailySale);
                TextView  monthlySale = (TextView)findViewById(R.id.monthlySale);
                TextView  annualySale = (TextView)findViewById(R.id.annualSale);
                String data[][];
                data = salabs.getArray(response,Statistics.this);
               //
                try {
                    if(data[0][1]!=null)
                        dailySale.setText("DailySale: "+data[0][1]+"/-");
                    else
                        dailySale.setText("DailySale:  0/-");
                    if(data[0][1]!=null)
                        monthlySale.setText("MonthlySale: "+data[1][1]+"/-");
                    else
                        monthlySale.setText("DailySale:  0/-");
                    if(data[0][1]!=null)
                        annualySale.setText("AnnualySale: "+data[2][1]+"/-");
                    else
                        annualySale.setText("DailySale:  0/-");


                } catch (Exception e) {
                    e.printStackTrace();
                }


//*/
            }
        };
        final salabssqlconnector salabssqlconnector= new salabssqlconnector(this,cmd);


      salabssqlconnector.execute("SELECT 1,sum(TotalPrice) as Sale, Day(CurrentDateTime) "+
                "FROM `CustomerTransiction` "+
                "WHERE Date(CurrentDateTime) = Date('"+s+"') "+
                "UNION "+
                "SELECT 2,sum(TotalPrice), Month(CurrentDateTime) "+
                "FROM `CustomerTransiction` "+
                "WHERE Month(CurrentDateTime) = Month('"+s+"') AND Year(CurrentDateTime) = Year('"+s+"') "+
                "UNION "+
                "SELECT 3,sum(TotalPrice), Year(CurrentDateTime) "+
                "FROM `CustomerTransiction` "+
                "WHERE Year(CurrentDateTime) = Year('"+s+"')","localhost","muklavalocal","muklava","shresthharsh");

//*/
    }
}
