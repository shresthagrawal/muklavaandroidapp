package com.example.unknown_joker.muklavaapp.Bill;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.CustomListAdapter;
import com.example.unknown_joker.muklavaapp.Product;
import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.salabs;
import com.example.unknown_joker.muklavaapp.salabssqlconnector;

import java.awt.font.TextAttribute;
import java.util.ArrayList;

public class BillView extends AppCompatActivity {
 int id;
    ListView lv;
    ArrayList<Product> arrayList;
    String data[][];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_view);
        id= salabs.getIntDefaults("BillId",this);
        lv = (ListView) findViewById(R.id.productList);
        arrayList = new ArrayList<>();


        salabssqlconnector.Command cmd = new salabssqlconnector.Command(){
            public void execute(String response){
                //do something
               // Toast.makeText(BillView.this, response, Toast.LENGTH_LONG).show();

                data = salabs.getArray(response,BillView.this);

                int size = data.length;
                //Toast.makeText(BillView.this, "total number:" + Integer.toString(data.length), Toast.LENGTH_LONG).show();

                for(int i=0; i<size;i++) {

                    arrayList.add(new Product("http://muklava.in/muklava/"+data[i][0]+".jpeg",data[i][1]+" "+data[i][2],data[i][3]+"/-" ));
                }
                if(size>0)
                {
                    TextView primaryDetails = findViewById(R.id.details);
                    primaryDetails.setText(data[0][5]+":"+data[0][4]+"");
                    TextView secondaryDetails = findViewById(R.id.extradetails);
                    secondaryDetails.setText("Total:"+data[0][7]+"  Discount:"+data[0][6]+"  Credit:"+data[0][8]);

                }
                CustomListAdapter adapter = new CustomListAdapter(BillView.this, R.layout.custom_list_layout, arrayList);
                lv.setAdapter(adapter);

            }
        };
        final salabssqlconnector salabssqlconnector= new salabssqlconnector(this,cmd);


        salabssqlconnector.execute("SELECT ns.ProductId, ns.Name, ns.PartyName, ns.Mrp, cs.Number, cs.Name, cs.Discount, cs.TotalPrice, cs.credit FROM NullStock as ns cross join CustomerTransiction as cs WHERE  ns.TransictionId = "+id+"  AND cs.TransictionId = "+id,"localhost","muklavalocal","muklava","shresthharsh");






        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
               // Toast.makeText(BillView.this, "total number:" + position, Toast.LENGTH_LONG).show();
                salabs.setDefaults("PhotoId",data[position][0],BillView.this);
                Intent intent = new Intent(BillView.this, PhotoView.class);
                startActivity(intent);
            }});


    }
}
