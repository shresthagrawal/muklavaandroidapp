package com.example.unknown_joker.muklavaapp;

/**
 * Created by Unknown-Joker on 4/19/2018.
 */


public class DetailInfo {

    private String sequence = "";
    private String name = "";

    public String getSequence() {
        return sequence;
    }
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
