package com.example.unknown_joker.muklavaapp.FireBase;

import android.util.Log;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.salabssqlconnector;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
       sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
      //  Toast.makeText(this,"hi",Toast.LENGTH_LONG).show();
        try {
            salabssqlconnector salabssqlconnector= new salabssqlconnector(this);
           salabssqlconnector.execute("insert into fcm_info set fcm_token='"+token+"' ON DUPLICATE KEY UPDATE fcm_token= fcm_token","localhost","fcmInfo","muklava","shresthharsh");
        } catch (Exception e) {

            e.printStackTrace();
        }
        // Add custom implementation, as needed.
    }
}