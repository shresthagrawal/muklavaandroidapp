package com.example.unknown_joker.muklavaapp;

/**
 * Created by Unknown-Joker on 4/19/2018.
 */
import android.arch.lifecycle.Lifecycle;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.CustomListAdapter;
import com.example.unknown_joker.muklavaapp.Product;
import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.salabs;
import com.example.unknown_joker.muklavaapp.salabsofflinedbms;

import java.util.ArrayList;


public class tab2dailystocks extends Fragment{
    ViewPager viewPager;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      rootView = inflater.inflate(R.layout.tab2dailystocks, container, false);
        //setRetainInstance(true);

        //Code to get the button from layout file

        return rootView;

    }
    ArrayList partyname =new ArrayList<>();
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
             partyname.clear();
            final ArrayList arrayList = new ArrayList<>();
            ListView lv = (ListView) rootView.findViewById(R.id.listViewpartyname);

            salabsofflinedbms salabsofflinedbms = new salabsofflinedbms(getContext());
            Cursor csr = salabsofflinedbms.query("select partyname, count(partyname) from product where (branch='"+ salabs.getDefaults("branch",getContext())+"') AND productname='"+salabs.getDefaults("itemnametodaysstock",getContext())+"' group by partyname");
            while(csr.moveToNext()) {
                partyname.add(csr.getString(0));
                arrayList.add(new Product("http://muklava.in/muklava/"+""+".jpeg",csr.getString(0),csr.getString(1) ));
            }

            CustomListAdapter adapter = new CustomListAdapter(
                    getActivity().getApplicationContext(), R.layout.custom_list_layout, arrayList);
            lv.setAdapter(adapter);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
               Toast.makeText(getContext(),Integer.toString(position), Toast.LENGTH_LONG).show();
                salabs.setDefaults("partynametodaysstock",partyname.get(position).toString(),getContext());
                viewPager = (ViewPager) getActivity().findViewById(
                        R.id.container);
                viewPager.setCurrentItem(2);
            }
        });
        } else {
            // Do your Work
        }
    }
}
