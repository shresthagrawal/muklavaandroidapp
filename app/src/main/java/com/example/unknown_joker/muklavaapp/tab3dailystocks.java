package com.example.unknown_joker.muklavaapp;

/**
 * Created by Unknown-Joker on 4/19/2018.
 */
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.Bill.BillView;
import com.example.unknown_joker.muklavaapp.Bill.ListOfBill;
import com.example.unknown_joker.muklavaapp.Bill.PhotoView;
import com.example.unknown_joker.muklavaapp.CustomListAdapter;
import com.example.unknown_joker.muklavaapp.Product;
import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.salabs;
import com.example.unknown_joker.muklavaapp.salabsofflinedbms;

import java.util.ArrayList;


public class tab3dailystocks extends Fragment{
    ViewPager viewPager;
    View rootView;
    ArrayList produtid =new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tab3dailystocks, container, false);
        return rootView;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            final ArrayList arrayList = new ArrayList<>();
            ListView lv = (ListView) rootView.findViewById(R.id.listviewproduct);

            salabsofflinedbms salabsofflinedbms = new salabsofflinedbms(getContext());

                String q ="select * from product where (branch='"+ salabs.getDefaults("branch",getContext())+"') AND (productname='"+salabs.getDefaults("itemnametodaysstock",getContext())+"') AND (partyname='" + salabs.getDefaults("partynametodaysstock", getContext()) +"')";
                Cursor csr = salabsofflinedbms.query(q);
            // Toast.makeText(getContext(),q,Toast.LENGTH_LONG).show();
             while (csr.moveToNext()) {
                 produtid.add(csr.getShort(0));
                 arrayList.add(new Product("http://muklava.in/muklava/"+csr.getString(0)+".jpeg",csr.getString(0)+" "+csr.getString(1)+" "+csr.getString(2),csr.getString(3)+"/-" ));
             }

            CustomListAdapter adapter = new CustomListAdapter(
                    getActivity().getApplicationContext(), R.layout.custom_list_layout, arrayList);
            lv.setAdapter(adapter);

           lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    Toast.makeText(getContext(), Integer.toString(position), Toast.LENGTH_LONG).show();
                    salabs.setDefaults("PhotoId", produtid.get(position).toString(), getContext());
                    Intent intent = new Intent(getContext(), PhotoView.class);
                    startActivity(intent);
                }
            });
        } else {
            // Do your Work
        }
    }

}
