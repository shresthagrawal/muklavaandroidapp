package com.example.unknown_joker.muklavaapp;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Unknown-Joker on 4/13/2018.
 */

public class salabssqlconnector extends AsyncTask<String,Void,String>
{
        Context ctx;
        static String Id="",response="";
        private Context mContext;
        public String rawdata=null;

    public interface Command{
        void execute(String r);
    }
        public salabssqlconnector(Context ctx)
        {
            this.ctx=ctx;



        }
        public static Command Cmd = null;
       public salabssqlconnector(Context ctx, Command cmd)
    {
        this.ctx=ctx;
        Cmd=cmd;


    }
        @Override
        protected void onPreExecute() {
        super.onPreExecute();
    }


        @Override
        protected String doInBackground(String... params) {



        String urls = "http://muklava.in/salabs/sqlconnector.php";
            try {
                if(params[5].equals("localNetwork")){ urls = params[6];
                 //   Toast.makeText(ctx,params[6],Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            //  Toast.makeText(ctx,result,Toast.LENGTH_LONG).show();
            String query=params[0];
            String host=params[1];
            String db= params[2];
            String user = params[3];
            String password = params[4];
            try {
                URL url =  new URL(urls);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data= URLEncoder.encode("query","UTF-8")+"="+ URLEncoder.encode(query,"UTF-8")+"&"+
                        URLEncoder.encode("host","UTF-8")+"="+ URLEncoder.encode(host,"UTF-8")+"&"+
                        URLEncoder.encode("db","UTF-8")+"="+ URLEncoder.encode(db,"UTF-8")+"&"+
                        URLEncoder.encode("user","UTF-8")+"="+ URLEncoder.encode(user,"UTF-8")+"&"+
                        URLEncoder.encode("password","UTF-8")+"="+ URLEncoder.encode(password,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();

                InputStream IS = httpURLConnection.getInputStream();

                InputStream inputStream1 = httpURLConnection.getInputStream();
                BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(inputStream1,"iso-8859-1"));

                String line="";
                response="";
                while ((line = bufferedReader1.readLine())!=null)
                {

                    response+=line;


                }
                bufferedReader1.close();
                inputStream1.close();
                httpURLConnection.disconnect();
                OS.close();
                IS.close();
                //Toast.makeText(ctx,response,Toast.LENGTH_LONG).show();
                return response;








            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            }







        return null;
    }

        @Override
        protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

        @Override
        protected void onPostExecute(String result) {
       //Toast.makeText(ctx,response,Toast.LENGTH_LONG).show();
        rawdata=response;
        salabs.setDefaults("queryresult",response,ctx);
        if(Cmd != null)
        {
           Cmd.execute(response);
        }

    }
    }

