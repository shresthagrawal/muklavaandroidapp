package com.example.unknown_joker.muklavaapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreateOrder extends AppCompatActivity {

    boolean check=true;
    EditText phonenumber;
    TextView welcome;
    String strphonenumber;
    salabssqlconnector sqlconnector;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_order);
        phonenumber= findViewById(R.id.edittxtnumber);
        welcome =findViewById(R.id.txtwelcome);
        sqlconnector= new salabssqlconnector(this);
        handler = new Handler();
    }
    public void onstartclicked(View view)
    {
       // doPhotoPrint();


           strphonenumber=phonenumber.getText().toString();
            sqlconnector.execute("Select Name from CustomerRecord where phonenumber='"+strphonenumber+"'","localhost","Muklava","muklava","shresthharsh");


            handler.postDelayed(new Runnable() {
                public void run() {
                    postsql();
                }
            }, 500);




    }

    public  void postsql() {

        if(salabs.getArray(sqlconnector.rawdata,this).length!=0) {
            check=false;
            Toast.makeText(this,"Welcome " + salabs.getArray(sqlconnector.rawdata, this)[0][0],Toast.LENGTH_LONG).show();
            Intent i = new Intent(this,FormsActivity.class);
            salabs.setDefaults("customernumber",strphonenumber,this);
            startActivity(i);

        }
        else {
            check=true;
            Toast.makeText(this,"Enter Valid Number",Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, customerentry.class);
            startActivity(i);
            salabs.setDefaults("customernumber","-1",this);
        }





    }

    private void doPhotoPrint() {
        View rootView = getWindow().findViewById(R.id.testlayout);

        PrintHelper photoPrinter = new PrintHelper(this);
        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
        photoPrinter.printBitmap("invoice", getScreenShot(rootView));
    }


    public static Bitmap getScreenShot(View view) {
        View screenView = view;
        screenView.setDrawingCacheEnabled(true);
        screenView.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    }

