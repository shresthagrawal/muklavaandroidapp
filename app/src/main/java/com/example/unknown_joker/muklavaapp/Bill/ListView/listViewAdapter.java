package com.example.unknown_joker.muklavaapp.Bill.ListView;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.unknown_joker.muklavaapp.Product;
import com.example.unknown_joker.muklavaapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class listViewAdapter  extends ArrayAdapter<element> {

    ArrayList<element> elements;
    Context context;
    int resource;

    public listViewAdapter(Context context, int resource, ArrayList<element> elements) {
        super(context, resource, elements);
        this.elements = elements;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.activity_list_view_adapter, null, true);

        }
        element element = getItem(position);


        TextView txtName = (TextView) convertView.findViewById(R.id.txtString1);
        txtName.setText(element.getString1());

        TextView txtPrice = (TextView) convertView.findViewById(R.id.txtString2);
        txtPrice.setText(element.getString2());

        return convertView;
    }}