package com.example.unknown_joker.muklavaapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class photolistview extends AppCompatActivity {

    ArrayList<Product> arrayList;
    ListView lv;
    salabssqlconnector salabssqlconnector4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photolistview);



        arrayList = new ArrayList<>();
        lv = (ListView) findViewById(R.id.photoListView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_main);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent i = new Intent(photolistview.this, SortActivity.class);
                        startActivity(i);
            }
        });







       String data[][]= salabs.getArray(salabs.getDefaults("queryresult",this),this);

        Toast.makeText(this,"total number:"+Integer.toString(data.length),Toast.LENGTH_LONG).show();
        int size = data.length;

        for(int i=0; i<size;i++) {
           arrayList.add(new Product("http://muklava.in/muklava/"+data[i][0]+".jpeg",data[i][0]+" "+data[i][1]+" "+data[i][2],data[i][3]+"/-" ));
        }
        CustomListAdapter adapter = new CustomListAdapter(
                getApplicationContext(), R.layout.custom_list_layout, arrayList);
        lv.setAdapter(adapter);


    }


}
