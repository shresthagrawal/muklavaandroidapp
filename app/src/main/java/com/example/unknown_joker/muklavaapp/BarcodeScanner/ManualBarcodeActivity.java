package com.example.unknown_joker.muklavaapp.BarcodeScanner;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.dailystockmanager;
import com.example.unknown_joker.muklavaapp.muklavafunctions;
import com.example.unknown_joker.muklavaapp.salabs;

public class ManualBarcodeActivity extends AppCompatActivity {
 TextView id;
 String server="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_manual_barcode);
        id = findViewById(R.id.productId);
        server= salabs.getDefaults("server",this);
        muklavafunctions.checkOfflineServer(this,server);
    }

    public void okPressed(View view) {

        try {
            muklavafunctions.deleteProductFromOfflineDbms1(this,Integer.parseInt(id.getText().toString()),server);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        Toast toast= Toast.makeText(getApplicationContext(),
                id.getText().toString(), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        id.setText("");



    }

    public void camView(View view) {
        Intent i= new Intent(this,BarcodeScanner.class);
        startActivity(i);
    }

    public void check(View view) {
        muklavafunctions.checkProduct(this,server,id.getText().toString());
    }
}
