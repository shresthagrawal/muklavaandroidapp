package com.example.unknown_joker.muklavaapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by shresth on 8/18/2017.
 */

public class salabsofflinedbms extends SQLiteOpenHelper {

    public static final String DATABASE_NAME ="muklava.db";
    public static final String DATABASE_TABLE ="product";



    public static final String T1COL_1 ="productid";
    public static final String T1COL_2 ="productname";
    public static final String T1COL_3 ="partyname";
    public static final String T1COL_4 ="mrp";
    public static final String T1COL_5 ="branch";
    public static final String T1COL_6 ="CurDateTime";
    public static final String T1COL_7 ="size";










    public salabsofflinedbms(Context context) {
        super(context,DATABASE_NAME, null, 1);
        //SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

      //  db.execSQL("CREATE TABLE "+DATABASE_TABLE+"(ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT,SURNAME TEXT,MARKS TEXT)");
        db.execSQL("CREATE TABLE "+DATABASE_TABLE+"("+T1COL_1+" VARCHAR(50) DEFAULT NULL,"
                    +T1COL_2+" VARCHAR(50) DEFAULT NULL,"
                    +T1COL_3+" VARCHAR(50) DEFAULT NULL,"
                    +T1COL_4+" VARCHAR(50) DEFAULT NULL,"
                    +T1COL_5+" VARCHAR(50) DEFAULT NULL,"
                    +T1COL_6+" VARCHAR(50) DEFAULT NULL,"
                    +T1COL_7+" VARCHAR(50) DEFAULT NULL)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

      //  db.execSQL("DROP TABLE IF EXISTS "+ DATABASE_TABLE);
       db.execSQL("DROP TABLE IF EXISTS "+ DATABASE_TABLE);
        onCreate(db);

    }


    public Cursor query(String query)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res= db.rawQuery(query,null);
        return  res;


    }

    public boolean InsertDataMasterTable(String productid, String productname, String partyname, String mrp,String branch,String CurDateTime,String size)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(T1COL_1,productid);
        contentValues.put(T1COL_2,productname);
        contentValues.put(T1COL_3,partyname);
        contentValues.put(T1COL_4,mrp);
        contentValues.put(T1COL_5,branch);
        contentValues.put(T1COL_6,CurDateTime);
        contentValues.put(T1COL_7,size);



        long result = db.insert(DATABASE_TABLE,null, contentValues);

        if(result==-1)
        {
            return false;
        }
        else
        {
            return  true;
        }


    }
    public void query(String query, int i)
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);

    }

}
