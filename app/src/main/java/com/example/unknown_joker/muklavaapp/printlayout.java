package com.example.unknown_joker.muklavaapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class printlayout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printlayout);
        TextView txtorderid = findViewById(R.id.txtorderid);
        txtorderid.setText(txtorderid.getText()+" "+FormsActivity.values[0]);
        TextView txtphonenumber = findViewById(R.id.txtnumber);
        txtphonenumber.setText(txtorderid.getText()+" "+FormsActivity.values[1]);
        String leftdata[]={
                "Full Length: "+FormsActivity.values[2],
                "Upper Chest: "+FormsActivity.values[3],
                "Chest: "+FormsActivity.values[4],
                "Upper Waist: "+FormsActivity.values[5],
                "Waist: "+FormsActivity.values[6],
                "Hip: "+FormsActivity.values[7],
                "Shoulder: "+FormsActivity.values[8]

        };


        ListView listviewleft = (ListView)findViewById(R.id.listviewleft);
        ListView listviewright = (ListView)findViewById(R.id.listviewright);
        listviewleft.setAdapter(new ArrayAdapter<String>(printlayout.this, R.layout.text, leftdata));



    }
}
