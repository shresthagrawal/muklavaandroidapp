package com.example.unknown_joker.muklavaapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.Bill.ListView.element;
import com.example.unknown_joker.muklavaapp.Bill.ListView.listViewAdapter;
import com.example.unknown_joker.muklavaapp.MainActivity.Statistics;

public class SortActivity extends AppCompatActivity {

    salabssqlconnector salabssqlconnector1,salabssqlconnector2,salabssqlconnector3,salabssqlconnector4;
    Spinner party,itemname;
    SeekBar skbarmin,skbarmax;
    int min,max;
    private String selectedparty = null;
    private String selectedproductname = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sort);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width =dm.widthPixels ;
        int height=dm.heightPixels;

        getWindow().setLayout((int)(width*.861),(int)(height*.51));



         party = (Spinner) findViewById(R.id.spinnerparty);
         itemname = (Spinner) findViewById(R.id.spinnername);
         skbarmin = (SeekBar) findViewById(R.id.seekbarmin);
         skbarmax = (SeekBar) findViewById(R.id.seekbarmax);

          party.setOnItemSelectedListener(new MyOnItemSelectedListener());
         itemname.setOnItemSelectedListener(new MyOnItemSelectedListener());

        salabssqlconnector.Command cmd = new salabssqlconnector.Command(){
            public void execute(String response){
                //do something
                methodcheck();
            }
        };
        salabssqlconnector1= new salabssqlconnector(this,cmd);
        salabssqlconnector1.execute("select distinct(partyname) from product","localhost","muklavalocal","muklava","shresthharsh");










        // perform seek bar change listener event used for getting the progress value
        skbarmin.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                TextView txt = findViewById(R.id.txtmin);
                txt.setText("Min: "+Integer.toString(progressChangedValue));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {

                min=progressChangedValue;
               // Toast.makeText(SortActivity.this, "Seek bar progress is :" + progressChangedValue,
                 //       Toast.LENGTH_SHORT).show();
            }
        });


        skbarmax.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                TextView txt = findViewById(R.id.txtmax);
                txt.setText("Max: "+Integer.toString(progressChangedValue));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                max=progressChangedValue;

                // Toast.makeText(SortActivity.this, "Seek bar progress is :" + progressChangedValue,
                //       Toast.LENGTH_SHORT).show();
            }
        });




       /* String productname[][]= salabs.sqlquery(this,"Select Distinct(productname) from product");
        Spinner spinner2 = (Spinner) findViewById(R.id.spinnername);
        ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item,onedfromtwod(productname));
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner2.setAdapter(spinnerArrayAdapter2);*/
    }

    public  void methodcheck()
    {
       // Toast.makeText(this,salabssqlconnector1.rawdata,Toast.LENGTH_LONG).show();

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,onedfromtwod(salabs.getArray(salabssqlconnector1.rawdata,this)));
        spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        party.setAdapter(spinnerArrayAdapter1);


        salabssqlconnector.Command cmd2 = new salabssqlconnector.Command(){
            public void execute(String response){
                //do something
                methodcheck2();
            }
        };
        salabssqlconnector2= new salabssqlconnector(this,cmd2);
        salabssqlconnector2.execute("select distinct(productname) from product","localhost","muklavalocal","muklava","shresthharsh");




    }

    public  void methodcheck2() {
       // Toast.makeText(this, salabssqlconnector2.rawdata, Toast.LENGTH_LONG).show();

        ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, onedfromtwod(salabs.getArray(salabssqlconnector2.rawdata, this)));
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        itemname.setAdapter(spinnerArrayAdapter2);

        salabssqlconnector.Command cmd3 = new salabssqlconnector.Command(){
            public void execute(String response){
                //do something
                methodcheck3();
            }
        };

        salabssqlconnector3= new salabssqlconnector(this,cmd3);
        salabssqlconnector3.execute("select max(mrp),min(mrp) from product","localhost","muklavalocal","muklava","shresthharsh");


    }

    public  void methodcheck3() {
        Toast.makeText(this, salabssqlconnector3.rawdata, Toast.LENGTH_LONG).show();
        skbarmin.setMax(Integer.parseInt(salabs.getArray(salabssqlconnector3.rawdata,this)[0][0]));
        //skbarmin.setMin(Integer.parseInt(salabs.getArray(salabssqlconnector3.rawdata,this)[0][1]));

        skbarmax.setMax(Integer.parseInt(salabs.getArray(salabssqlconnector3.rawdata,this)[0][0]));
        skbarmax.setProgress(Integer.parseInt(salabs.getArray(salabssqlconnector3.rawdata,this)[0][0]));
        max=Integer.parseInt(salabs.getArray(salabssqlconnector3.rawdata,this)[0][0]);
       // skbarmax.setMin(Integer.parseInt(salabs.getArray(salabssqlconnector3.rawdata,this)[0][1]));
       }


        public String[] onedfromtwod(String data[][])
    {
        //+1 is done for blank string
        String result[]= new String[data.length+1];
        result[0]="";
        for(int i=1;i<data.length+1;i++)
        {
          result[i]=data[i-1][0];

        }
        return result;
    }



    public void search(View view)
    {
        String query="select * from product where (mrp<="+max+") AND (mrp>="+min+")";
        if(!selectedproductname.equals(""))
        {
            query=query+"AND (productname ='"+selectedproductname+"')";
        }
        if(!selectedparty.equals(""))
        {
            query=query+"AND (partyname ='"+selectedparty+"')";
        }

        salabssqlconnector.Command cmd4 = new salabssqlconnector.Command(){
            public void execute(String response){
                //do something
                methodcheck4();
            }
        };
        salabssqlconnector4= new salabssqlconnector(this,cmd4);

        salabssqlconnector4.execute(query,"localhost","muklavalocal","muklava","shresthharsh");





    }
    void methodcheck4()
    {
        salabs.setDefaults("queryresult",salabssqlconnector4.rawdata,this);
        Intent i = new Intent(this, photolistview.class);
        startActivity(i);
    }

    public class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            String selectedItem = parent.getItemAtPosition(pos).toString();

            //check which spinner triggered the listener
            switch (parent.getId()) {
                //country spinner
                case R.id.spinnerparty:
                    //make sure the country was already selected during the onCreate
                    if(selectedparty != null){
                        Toast.makeText(parent.getContext(), "party you selected is " + selectedItem,
                                Toast.LENGTH_LONG).show();
                    }
                    selectedparty = selectedItem;
                    break;
                //animal spinner
                case R.id.spinnername:
                    //make sure the animal was already selected during the onCreate
                    if(selectedItem != null){
                        Toast.makeText(parent.getContext(), "name you selected is " + selectedItem,
                                Toast.LENGTH_LONG).show();
                    }
                    selectedproductname= selectedItem;
                    break;
            }


        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }
}
