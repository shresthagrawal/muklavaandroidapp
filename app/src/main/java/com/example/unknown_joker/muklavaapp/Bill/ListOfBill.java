package com.example.unknown_joker.muklavaapp.Bill;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.unknown_joker.muklavaapp.CustomListAdapter;
import com.example.unknown_joker.muklavaapp.Product;
import com.example.unknown_joker.muklavaapp.R;
import com.example.unknown_joker.muklavaapp.SortActivity;
import com.example.unknown_joker.muklavaapp.Splash;
import com.example.unknown_joker.muklavaapp.photolistview;
import com.example.unknown_joker.muklavaapp.salabs;
import com.example.unknown_joker.muklavaapp.salabssqlconnector;

import java.util.ArrayList;

public class ListOfBill extends AppCompatActivity {
    ListView lv;
    ArrayList<Product> arrayList;
    String data[][];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_bill);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lv = (ListView) findViewById(R.id.BillList);
        arrayList = new ArrayList<>();

        salabssqlconnector.Command cmd = new salabssqlconnector.Command(){
            public void execute(String response){
                //do something
                data = salabs.getArray(response,ListOfBill.this);

                int size = data.length;
                Toast.makeText(ListOfBill.this, "total number:" + Integer.toString(data.length), Toast.LENGTH_LONG).show();

                for(int i=0; i<size;i++) {

                    arrayList.add(new Product("",data[i][1]+":"+data[i][0],data[i][3]+"/-" ));
                }
                CustomListAdapter adapter = new CustomListAdapter(ListOfBill.this, R.layout.custom_list_layout, arrayList);
                lv.setAdapter(adapter);

            }
        };
        final salabssqlconnector salabssqlconnector= new salabssqlconnector(this,cmd);


       salabssqlconnector.execute("SELECT Number, Name, TransictionId, TotalPrice, credit FROM CustomerTransiction WHERE  CurrentDateTime >= (CURDATE()-10) order by CurrentDateTime DESC ","localhost","muklavalocal","muklava","shresthharsh");






        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(ListOfBill.this, "total number:" + position, Toast.LENGTH_LONG).show();
                salabs.setDefaults("BillId",data[position][2],ListOfBill.this);
                Intent intent = new Intent(ListOfBill.this, BillView.class);
                startActivity(intent);
            }});




    }

}























