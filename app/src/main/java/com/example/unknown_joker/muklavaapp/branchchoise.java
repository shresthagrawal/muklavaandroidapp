package com.example.unknown_joker.muklavaapp;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import java.util.ArrayList;

public class branchchoise extends AppCompatActivity {
    Spinner spinnerbranchchoise;
    ArrayList arrayList= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branchchoise);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width =dm.widthPixels ;
        int height=dm.heightPixels;

        getWindow().setLayout((int)(width*.961),(int)(height*.51));
        arrayList.add("Pandri");
        String []array =(String[]) arrayList.toArray(new String[arrayList.size()]);



        spinnerbranchchoise = (Spinner) findViewById(R.id.spinnerbranchchoice);
        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,array);
        spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerbranchchoise.setAdapter(spinnerArrayAdapter1);
        spinnerbranchchoise.setOnItemSelectedListener(new MyOnItemSelectedListener());

    }

    public void ok(View view) {
        Intent i= new Intent(this,dailystockmanager.class);
        startActivity(i);
    }

    public void send(View view) {
        muklavafunctions.sendReport(this,"Pandri");
        Intent i= new Intent(this,dailystockmanager.class);
        startActivity(i);
    }
    ProgressBar pb;
    public void Sync(View view) {
        pb = findViewById(R.id.progressBar4);
        EditText serverName= findViewById(R.id.serverName);
        salabs.setDefaults("server",serverName.getText().toString(),this);
        muklavafunctions.getdataonlinetoofline(this,pb,serverName.getText().toString());
    }

    public void Start(View view) {
        muklavafunctions.startStockManagement(this,pb);
    }

    public class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            String selectedItem = parent.getItemAtPosition(pos).toString();


            switch (parent.getId()) {

                case R.id.spinnerbranchchoice:
                   salabs.setDefaults("branch",selectedItem,branchchoise.this);
                    break;


            }


        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }
}
