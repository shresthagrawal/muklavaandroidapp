package com.example.unknown_joker.muklavaapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.jar.Attributes;

public class FormsActivity extends AppCompatActivity {
    EditText FullLength;
    EditText UpChest;
    EditText Chest;
    EditText UpWaist;
    EditText Waist;
    EditText Hip;
    EditText Shoulder;
    EditText Sleeves;
    EditText ArmHole;
    EditText Work;
    EditText FabricDetails;
    EditText Dye;
    EditText Hanging;
    EditText OrderDate;
    EditText MuklavaDate;
    EditText DueDate;
    EditText Status;
    EditText AdditionalInformation;
    Button Proceed;
    String []Answers;
    salabssqlconnector customerlastdata,maxproductid,maxorderid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forms);

        FullLength = findViewById(R.id.Full_Length);
        UpChest = findViewById(R.id.Upper_Chest);
        Chest = findViewById(R.id.Chest);
        UpWaist = findViewById(R.id.Upper_Waist);
        Waist = findViewById(R.id.Waist);
        Hip = findViewById(R.id.Hip);
        Shoulder = findViewById(R.id.Shoulder);
        Sleeves = findViewById(R.id.Sleeves);
        ArmHole = findViewById(R.id.Arm_Hole);
        Work = findViewById(R.id.Work);
        FabricDetails = findViewById(R.id.Fabric_Details);
        Dye = findViewById(R.id.Dye);
        Hanging = findViewById(R.id.Hanging);
        DueDate = findViewById(R.id.Due_Date);
        AdditionalInformation = findViewById(R.id.Additional_Information);
        Proceed = findViewById(R.id.proceed);

        String strphonenumber=(salabs.getDefaults("customernumber",this));
        if(!strphonenumber.equals("-1"))
        {
            maxproductid= new salabssqlconnector(this);
            maxproductid.execute("Select Max(orderid) from OrderList where (phonenumber='"+(strphonenumber)+"')","localhost","Muklava","muklava","shresthharsh");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    postsql(0);
                }
            }, 500);
        }

    }

    private void postsql(int a) {

        if(a==0) {
            String data[][] = salabs.getArray(maxproductid.rawdata, this);
            customerlastdata = new salabssqlconnector(this);
            customerlastdata.execute("Select * from OrderList where (orderid='" + data[0][0] + "')", "localhost", "Muklava", "muklava", "shresthharsh");

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    postsql(1);
                }
            }, 500);
        }
        if(a==1)
        {
            String data[][] = salabs.getArray(customerlastdata.rawdata, this);
            FullLength.setText(data[0][5]);
            UpChest.setText(data[0][6]);
            Chest.setText(data[0][7]);
            UpWaist.setText(data[0][8]);
            Waist.setText(data[0][9]);
            Hip.setText(data[0][10]);
            Shoulder.setText(data[0][11]);
            Sleeves.setText(data[0][12]);
            ArmHole.setText(data[0][13]);



        }
        if(a==2)
        {
             String data[][] = salabs.getArray(maxorderid.rawdata, this);
             Toast.makeText(this,data[0][0],Toast.LENGTH_LONG).show();
            values [0] =data[0][0];
            values [1] =salabs.getDefaults("customernumber",this);
            values [2] =FullLength.getText().toString();
            values [3] =UpChest.getText().toString();
            values [4] = Chest.getText().toString();
            values [5] =UpWaist.getText().toString();
            values [6]=         Waist.getText().toString();
            values [7]=         Hip.getText().toString();
            values [8]=        Shoulder.getText().toString();
            values [9]=       Sleeves.getText().toString();
            values [10]=        ArmHole.getText().toString();
            values [11] =     Work.getText().toString();
            values [12]=       FabricDetails.getText().toString();
            values [13]=      Dye.getText().toString();
            values [14]=       Hanging.getText().toString();
            values [15]=       DueDate.getText().toString();
            values [16]=       AdditionalInformation.getText().toString();

            salabs.setDefaults("maxorderid",data[0][0],this);
            Intent test = new Intent(this,printlayout.class);
            startActivity(test);
           // Intent intent= new Intent(this,Upload.class);
           // startActivity(intent);




        }


    }
    public static String values[]= new String[17];

    public void Proceed(View view) {

        maxproductid= new salabssqlconnector(this);
        maxproductid.execute("INSERT INTO OrderList SET phonenumber='"+salabs.getDefaults("customernumber",this)+"'," +
                             "deliverydate='"+DueDate.getText().toString()+"',"+
                             "fulllength='"+FullLength.getText().toString()+"',"+
                             "upperchest='"+UpChest.getText().toString()+"',"+
                             "chest='"+Chest.getText().toString()+"',"+
                             "upperwaist='"+UpWaist.getText().toString()+"',"+
                             "waist='"+Waist.getText().toString()+"',"+
                             "hip='"+Hip.getText().toString()+"',"+
                             "shoulder='"+Shoulder.getText().toString()+"',"+
                            "sleeves='"+Sleeves.getText().toString()+"',"+
                            "armhole='"+ArmHole.getText().toString()+"',"+
                            "work='"+Work.getText().toString()+"',"+
                            "fabricdetails='"+FabricDetails.getText().toString()+"',"+
                            "dye='"+Dye.getText().toString()+"',"+
                            "hanging='"+Hanging.getText().toString()+"',"+
                            "additionalinformation='"+AdditionalInformation.getText().toString()+"',"+
                            "orderbyid='1'"
                            ,"localhost","Muklava","muklava","shresthharsh");

        maxorderid= new salabssqlconnector(this);
        maxorderid.execute("select max(orderid) from OrderList","localhost","Muklava","muklava","shresthharsh");





        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            public void run() {
                postsql(2);
            }
        }, 500);


    }

}
